//
//  User.swift
//  CustomeCellUsingXib
//
//  Created by BS23 on 5/31/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    

    var  id = String()
    var name = String()
    var email = String()
    var address = String()
    var gender = String()
    var phone: UserPhone
    
    
    init(UserJSON : JSON) {
        
        id = UserJSON["id"].stringValue
        name = UserJSON["name"].stringValue
        email = UserJSON["email"].stringValue
        address = UserJSON["address"].stringValue
        gender = UserJSON ["gender"].stringValue
        
        phone = UserPhone.init(UserPhoneJSON: UserJSON["phone"])
        
    }
}

class UserPhone {
    
    var mobile = String()
    var home = String()
    var office = String()
    
    init(UserPhoneJSON : JSON ) {
        mobile = UserPhoneJSON["mobile"].stringValue
        home = UserPhoneJSON["home"].stringValue
        office = UserPhoneJSON ["office"].stringValue
    }
    

}
