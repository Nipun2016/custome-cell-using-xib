//
//  RequestManager.swift
//  CustomeCellUsingXib
//
//  Created by BS23 on 5/31/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

//Delegate
protocol RequestManagerDelegates
{
    func getUserData(data: [User], isSuccess: Bool)
}

class RequestManager: NSObject {

    static let sharedInstance :  RequestManager = {
        let instance = RequestManager()
        return instance
        
    }()
    
    
    var delegate : RequestManagerDelegates?
    var users = [User]()
    
    func getData()
    {
        var dataResponse : JSON = JSON.null
        
        let url = URL (string: "http://api.androidhive.info/contacts/")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        
        Alamofire.request(request).responseJSON { responds in
            
            switch responds.result {
            case .success(let data):
                print("Success --> \(data)")
                dataResponse = JSON(data)
                
                for i in 0..<dataResponse["contacts"].count
                {
                    let singleDataObject = User(UserJSON: dataResponse["contacts"][i])
                    self.users.append(singleDataObject)
                }
                
                self.delegate?.getUserData(data: self.users, isSuccess: true)
                
            case .failure(let error):
                print("Error \(error)")
            }
            
        }
    }
    
}
