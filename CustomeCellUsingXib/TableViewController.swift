//
//  TableViewController.swift
//  CustomeCellUsingXib
//
//  Created by BS23 on 5/31/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import MBProgressHUD

class TableViewController: UITableViewController, RequestManagerDelegates {

    @IBOutlet var uiTableView: UITableView!
    var users = [User]()
    var isSuccess : Bool = false
    var spinningActivity : MBProgressHUD?

    

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        tableView.register(UINib(nibName: "TableViewCellOne", bundle: nil), forCellReuseIdentifier: "TableViewCellOne")

        self.Loding()
        RequestManager.sharedInstance.getData()
        
    }
    
    
    
// MARK: - Spinning Activity
   
    
    func Loding()
    {
        spinningActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinningActivity?.label.text = "...Loading..."
        spinningActivity?.detailsLabel.text = "Keep wait."
        //loading?.mode = .determinateHorizontalBar
        spinningActivity?.isUserInteractionEnabled = false
    }
    
 // MARK: - View Methods
    
    
    override func viewWillAppear(_ animated: Bool) {
        RequestManager.sharedInstance.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        RequestManager.sharedInstance.delegate = nil
    }
    
    
    func getUserData(data: [User], isSuccess: Bool)
    {
        
        self.users = data
        self.isSuccess = isSuccess
        if (isSuccess)
        {
            self.spinningActivity?.hide(animated: true)
        }
        self.uiTableView.reloadData()
    }
    
    
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return users.count
    }
    
   
    var flag = 0
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellOne", for: indexPath) as? TableViewCellOne


        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("TableViewCellOne", owner: self, options: nil)?.first as? TableViewCellOne
            
        }
        
        
        
        let referance = users[indexPath.row]
        
        cell?.idLabel.text = referance.id
        cell?.nameLable.text = referance.name
        cell?.emailLable.text = referance.email
        cell?.addressLabel.text = referance.address
        cell?.gebderLabel.text = referance.gender
        
        cell?.mobileLabel.text = referance.phone.mobile
        cell?.homeLabel.text = referance.phone.home
        cell?.officeLabel.text = referance.phone.office
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 320
    }
}
